export default function sendRequest(args) {
  let req = new XMLHttpRequest();
  req.open(args.method, args.url, args.async);
  req.send(args.body);
  req.onreadystatechange = () => {
    if (req.readyState == 4) {
      if (req.status == 200) {
        args.callback(null, req.responseText);
      } else {
        switch (req.status) {
          case 404:
            args.callback({ errMsg: "Invalid url" }, null);
            break;
          default:
            args.callback({ errMsg: "Could not load recourses" }, null);
        }
      }
    }
  };
}
