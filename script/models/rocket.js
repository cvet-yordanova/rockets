import random from "../helpers/random-num.js";
export default class Rocket {
  constructor(gameWidth, gameHeight) {
    this.gameWidth = gameWidth;
    this.gameHeight = gameHeight;

    //should be 1
    this.speedUp = 20;

    this.speed = 1;
    this.launched = false;
    this.activeStage = "first_stage";

    this.image = document.getElementById("rocket_img");
    this.thrust = document.getElementById("thrust");
    this.bottom = document.getElementById("rocket_bottom");

    this.fps = 60;
    this.alpha = 1;

    this.arrived = false;
    this.lostBottom = false;
  }

  setStartBodyPosX(posX) {
    this.startBodyPosX = posX;
    this.bodyPositionX = this.startBodyPosX;
  }

  setStartBodyPosY(posY) {
    this.startBodyPosY = posY;
    this.bodyPositionY = this.startBodyPosY;
  }

  setStartThrustPosX(posX) {
    this.startThrustPosX = posX;
    this.thrustPositionX = this.startThrustPosX;
  }

  setStartThrustPosY(posY) {
    this.startThrustPosY = posY;
    this.thrustPositionY = this.startThrustPosY;
  }

  initializeMeasures() {
    this.startHeight = this.height.meters;
    this.startWidth = this.diameter.meters * 7;

    this.startThrustWidth = this.diameter.meters * 6;
    this.startThrustHeight = this.startThrustWidth * 2;

    this.widthCanvas = this.startWidth;
    this.heightCanvas = this.startHeight;
    this.thrustWidth = this.startThrustWidth;
    this.thrustHeight = this.startThrustHeight;
    this.startFuel =
      this["first_stage"]["fuel_amount_tons"] +
      this["second_stage"]["fuel_amount_tons"];

    this.fuel = this.startFuel;

    this.totalDistance =
      this.fps *
      (this["first_stage"]["fuel_amount_tons"] / this.speedUp +
        this["second_stage"]["fuel_amount_tons"] / this.speedUp);
    this.distancePassed = 0;
  }

  draw(ctx) {
    ctx.save();
    ctx.globalAlpha = this.alpha;

    ctx.drawImage(
      this.thrust,
      this.thrustPositionX,
      this.thrustPositionY,
      this.thrustWidth,
      this.thrustHeight
    );
    ctx.drawImage(
      this.image,
      this.bodyPositionX,
      this.bodyPositionY,
      this.widthCanvas,
      this.heightCanvas
    );
    ctx.restore();
  }

  update(rocketMoving) {
    if (this.launched) {
      this.distancePassed += 1;
    }

    if (rocketMoving && this.bodyPositionY >= 0) {
      this.bodyPositionY -= this.speed;
      this.thrustPositionY -= this.speed;
    }

    this.thrustHeight = this.startThrustHeight + random.jump(10);
    this.widthCanvas = this.startWidth + random.jump(2);
    this.heightCanvas = this.startHeight + random.jump(2);

    if (
      this.activeStage == "first_stage" &&
      this.fuel < this["first_stage"]["fuel_amount_tons"]
    ) {
      this.activeStage = "second_stage";
      this.image = document.getElementById("rocket_top");
      this.thrustPositionY -= this.startHeight - this.startHeight * 0.75;

      this.startHeight = this.startHeight * 0.75;
      this.heightCanvas = this.startHeight;
    }
  }

  restartRocket() {
    this.launched = false;
    this.arrived = false;
    this.activeStage = "first_stage";
    this.image = document.getElementById("rocket_img");
    this.alpha = 1;
    this.initializeMeasures();
    clearInterval(this.fuelInterval);
  }

  consumeFuel() {
    this.launched = true;

    this.fuel -= this.speedUp;

    if (this.activeStage == "second_stage") {
      this.alpha = (this.fuel / this.startFuel) * 2;
    }
    if (this.fuel < 0) {
      this.fuel = 0;
      this.arrived = true;
    }
  }

  setInterval() {
    this.fuelInterval = setInterval(() => {
      this.consumeFuel();
    }, 1000);
  }
}
