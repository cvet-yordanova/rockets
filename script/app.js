import Rocket from "./models/rocket.js";

import sendRequest from "./helpers/request.js";
import Game from "./models/game.js";
import Background from "./models/background.js";
import UI from "./models/ui.js";

const GAME_WIDTH = 1000;
const GAME_HEIGHT = 600;

("use strict");

let rockets = [];
let canvas = document.getElementById("game_screen");
let ctx = canvas.getContext("2d");

let game;
let ui;

sendRequest({
  method: "GET",
  url: "https://api.spacexdata.com/v2/rockets",
  async: true,
  body: null,
  callback: initGame
});

function initGame(err, obj) {
  if (err) {
    console.error(err);
    return;
  }

  createRockets(obj).then(() => {
    game = createGame();
    ui = new UI(game);
    gameLoop();
  });
}

let createRockets = function(obj) {
  return new Promise(function(resolve, rej) {
    let rocketsJson = JSON.parse(obj);
    for (let i = 0; i < rocketsJson.length; i++) {
      let newRocket = new Rocket(GAME_WIDTH, GAME_HEIGHT);
      Object.assign(newRocket, rocketsJson[i]);

      rockets.push(newRocket);

      if (i === rocketsJson.length - 1) {
        resolve(rockets);
      }
    }
  });
};

let createGame = function() {
  return new Game(
    GAME_WIDTH,
    GAME_HEIGHT,
    rockets,
    new Background(ctx, GAME_WIDTH, GAME_HEIGHT)
  );
};

let gameLoop = function() {
  ctx.clearRect(0, 0, GAME_WIDTH, GAME_HEIGHT);
  game.update();
  game.draw(ctx);
  ui.displaySideInfo();

  requestAnimationFrame(gameLoop);
};
